
from Trax.Utils.Logging.Logger import Log

from Projects.GOOGLEUS.Utils.KPIToolBox import GOOGLEUSToolBox, log_runtime

__author__ = 'Ortal'


class GOOGLEUSGenerator:

    def __init__(self, data_provider, output):
        self.data_provider = data_provider
        self.output = output
        self.project_name = data_provider.project_name
        self.session_uid = self.data_provider.session_uid
        self.tool_box = GOOGLEUSToolBox(self.data_provider, self.output)

    @log_runtime('Total Calculations', log_start=True)
    def main_function(self):
        """
        This is the main KPI calculation function.
        It calculates the score for every KPI set and saves it to the DB.
        """
        if self.tool_box.scif.empty:
            Log.warning('Scene item facts is empty for this session')
        for kpi_set_fk in self.tool_box.kpi_static_data['kpi_set_fk'].unique().tolist():
            score = self.tool_box.main_calculation(set_fk=kpi_set_fk)
            if score is not None:
                self.tool_box.write_to_db_result(kpi_set_fk, score, self.tool_box.LEVEL1)
        self.tool_box.calculate_flat_kpis()
        self.tool_box.commit_results_data()

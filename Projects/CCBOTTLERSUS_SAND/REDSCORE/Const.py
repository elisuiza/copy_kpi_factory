
__author__ = 'ilanp'


class Const(object):

    # sheet names
    KPIS_SHEET_NAME = 'KPIs'
    SURVEY_SHEET_NAME = 'Survey'
    SOS_NAME = 'SOS'
    SKU_AVAILABILITY_SHEET_NAME = 'SKU_Availability'
    SCENE_AVAILABILITY_SHEET_NAME = 'SCENE_Availability'
    SURVEY_SHEET_QUESTION_PK_FIELD = 'Survey Q ID'
    SURVEY_SHEET_ANSWER_FIELD = 'Accepted Answer'
    EXCLUSION_SHEET = 'SKU_Exclusion'
    SHEET = 'Sheet'
    STORE_ATT15 = 'Store Attribute - Attr15'

    # const
    RED_SCORE = 'Red SCORE'
    KPI_GROUP = 'KPI Group'
    KPI_NAME = 'KPI Name'
    PRODUCT_CODES = 'Product EAN Code'
    SCENE_TYPE = 'SCENE TYPE (TEMPLATE NAME)'
    SCENE_TYPE2 = 'Scene Type'
    SCENE_TYPE_GROUP = 'Scene Type Group (Template Group)'
    AVAILABILITY_IMPULSE_ZONE = 'Impulze zone availability'
    WEIGHT = 'Weight'
    TEST_GROUP_CONDITION = 'Test Group Condition'
    CORE_BRANDS = '20oz SSD core brands in Impulse Zone Cooler'
    TRADEMARK = 'Trademark (att2)'
    PACKAGE_TYPE ='Package Type (att3)'
    PRODUCT_CATEGORY = 'SSD Still (att4)'
    CONDITION = 'Condition'
    DISPLAY_NAME = 'Display Name'
    STORE_TYPE = 'Store Type'
    MANUFACTURE = 'Manufacturer'
    BRAND_NAME = 'Brand Name'
    SUB_CATEGORY = 'Sub Category'
    NUM_OF_SUB_PACKAGES = 'Number of Sub Packages'
    ENTITY_TYPE_NUMERATOR = 'Entity Type Numerator'
    NUMERATOR = 'Numerator'
    CATEGORY = 'Category'
    BRAND = 'Brand'
    TARGET = 'Target (%)'
    TARGET2 = 'Target'
    INCREMENTAL = 'Incremental'

    #test group condition
    ALL_PASSED = 'All Passed'